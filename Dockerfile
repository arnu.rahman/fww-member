# BUILDER
FROM node:18-alpine AS builder

RUN adduser -D -g '' apps

WORKDIR /usr/src/app

COPY --chown=apps:apps package*.json ./

COPY --chown=apps:apps . .

RUN npm config set strict-ssl && \
  npm ci --legacy-peer-deps --ignore-scripts && \
  npm run build
RUN npm ci --legacy-peer-deps --only=production --ignore-scripts && npm cache clean --force

USER apps

# RUNNER
FROM node:18-alpine AS runner

RUN adduser -D -g '' apps

WORKDIR /app

COPY --chown=apps:apps --from=builder /usr/src/app/node_modules ./node_modules
COPY --chown=apps:apps --from=builder /usr/src/app/dist ./dist

ARG env
ENV ENVIRONMENT $env

USER apps
# EXPOSE 3001 3001
CMD [ "node", "dist/main.js" ]
