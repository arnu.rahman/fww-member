import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { ConfigModule } from '@nestjs/config';
import { AuthConfig } from '../config/auth.config';
import { CognitoStrategy } from './cognito.strategy';
import { BasicStrategy } from './auth-basic.strategy';

@Module({
  imports: [PassportModule, ConfigModule],
  providers: [BasicStrategy, AuthConfig, CognitoStrategy],
  exports: [AuthConfig],
})
export class AuthModule {}
