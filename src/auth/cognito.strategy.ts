import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { passportJwtSecret } from 'jwks-rsa';
import { AuthConfig } from '../config/auth.config';

@Injectable()
export class CognitoStrategy extends PassportStrategy(Strategy) {
  constructor(private authConfig: AuthConfig) {
    super({
      secretOrKeyProvider: passportJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `${authConfig.authority}/.well-known/jwks.json`,
      }),

      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      audience: authConfig.clientId,
      issuer: authConfig.authority,
      algorithms: ['RS256'],
    });
  }

  public async validate(payload: any) {
    const cognitoId = payload?.sub;

    if (!cognitoId) {
      throw new UnauthorizedException();
    }

    return {
      cognitoId: payload.sub,
      username: payload['cognito:username'],
      email: payload.email,
    };
  }
}
