export interface IStatusMember {
  id: string;
  isBlacklist: boolean;
}
