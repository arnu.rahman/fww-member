export interface ITokenLogin {
  accessToken: string;
  refreshToken: string;
}
