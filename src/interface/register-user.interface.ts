export interface IRegisterUser {
  cognitoId: string;
  username: string;
  name: string;
  email: string;
  phone: string;
}
