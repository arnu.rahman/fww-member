import { PickType } from '@nestjs/swagger';
import { MemberDto } from './member.dto';

export class CognitoUserDto extends PickType(MemberDto, [
  'cognitoId',
  'email',
  'username',
]) {}
