import { ApiProperty } from '@nestjs/swagger';
import { MemberExludePasswordDto } from '../base/member-without-password.dto';
import { BaseResponseDto } from './base.response.dto';

export class RegisterResponseDto extends BaseResponseDto {
  constructor(
    statusCode: number,
    message: string,
    data: MemberExludePasswordDto,
  ) {
    super(statusCode, message);
    this.data = data;
  }

  @ApiProperty({ example: 201 })
  statusCode: number;

  @ApiProperty({ example: 'This is sample message register successfully' })
  message: string;

  @ApiProperty({ type: MemberExludePasswordDto })
  data: MemberExludePasswordDto;
}
