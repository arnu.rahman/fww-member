import { PartialType, PickType } from '@nestjs/swagger';
import { MemberDto } from '../base/member.dto';

export class UpdateProfileRequestDto extends PartialType(
  PickType(MemberDto, [
    'email',
    'phone',
    'name',
    'identityNumber',
    'birthDate',
  ]),
) {}
