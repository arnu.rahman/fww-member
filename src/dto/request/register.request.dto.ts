import { PickType } from '@nestjs/swagger';
import { MemberDto } from '../base/member.dto';

export class RegisterRequestDto extends PickType(MemberDto, [
  'username',
  'password',
  'email',
  'name',
  'phone',
]) {}
