import { PickType } from '@nestjs/swagger';
import { MemberDto } from '../base/member.dto';

export class LoginRequestDto extends PickType(MemberDto, [
  'username',
  'password',
]) {}
