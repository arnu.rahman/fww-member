import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Member {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'uuid', unique: true })
  cognitoId: string;

  @Column({ name: 'identity_number', length: 16, nullable: true, unique: true })
  identityNumber: string;

  @Column({ length: 50, nullable: true })
  name: string;

  @Column({ length: 20, unique: true })
  username: string;

  @Column({ length: 50 })
  email: string;

  @Column({ length: 20 })
  phone: string;

  @Column({ name: 'birth_date', type: 'date', nullable: true })
  birthDate: string;

  @Column({ length: 10, nullable: true })
  status: string;

  @CreateDateColumn()
  created_at: string;

  @UpdateDateColumn()
  updated_at: string;

  @DeleteDateColumn()
  deleted_at: string;
}
